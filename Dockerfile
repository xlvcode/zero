FROM node:11-alpine
RUN mkdir -p /app
WORKDIR /app
COPY package*.json /app/
RUN npm install
RUN npm install pm2 -g
#RUN npm --productiondocker
COPY . .
CMD ["npm","run","dev"]
#CMD ["pm2-runtime","start","ecosystem.config.js"]
