const mongoose = require('mongoose');
let Schema = mongoose.Schema;

const lesson = new Schema({
    name:{
        type: String,
        required: true,
    },
    date:{
        type: Date,
        defaultValue:Date.now(),
    }
});
module.exports = mongoose.model('lesson', lesson);