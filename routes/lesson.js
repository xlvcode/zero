const express = require('express');
const router = express.Router();
const lesson = require('../models/lessonModel');

/* GET lesson list. */
router.get('/', function(req, res, next) {
    lesson.find({}, (err, tasks)=>res.status(200).send(tasks));
});
router.post('/', function (req, res, next) {
    let item = new lesson({
       name: req.body.name
    });
    item.save((err, item)=>{
        if(err){
            res.status(200).send(err);
        }else{
            res.status(200).json({
                "status":"Ok",
                "value": item
            });
        }
    });
});
// router.update('/:lessonId', (req, res)=>{
//     let id = req.param.lessonId;
//     lesson.find()
// });

module.exports = router;
